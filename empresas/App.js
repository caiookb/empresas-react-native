/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';

import Companies from './src/views/company/Companies';
import Login from './src/views/login/Login';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {AuthController, StoreController} from './src/controllers';
import {Easing} from 'react-native';
import {Spinner} from './src/commom-components';

import CompanyDetail from './src/views/company-detail/CompanyDetail';
import {UserActions} from './src/libs/redux/actions';

const Stack = createStackNavigator();
const dispatch = StoreController.dispatch();

const App = (props) => {
  const {isAuth} = props;
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    AuthController.checkAuth().then((res) => {
      setLoading(false);
      return res
        ? dispatch(UserActions.isAuth(true))
        : dispatch(UserActions.isAuth(false));
    });
  }, []);

  const config = {
    animation: 'spring',
    config: {
      stiffness: 1000,
      damping: 100,
      mass: 3,
      overshootClamping: false,
      restDisplacementeThreshold: 0.01,
      restSpeedThreshold: 0.01,
    },
  };

  const closeConfig = {
    animation: 'timing',
    config: {
      duration: 300,
      easing: Easing.linear,
    },
  };

  return loading ? (
    <Spinner />
  ) : (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={'login'}>
        {!isAuth ? (
          <Stack.Screen
            name={'login'}
            component={Login}
            options={{headerShown: false}}
          />
        ) : (
          <React.Fragment>
            <Stack.Screen
              name={'companies'}
              component={Companies}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name={'company'}
              component={CompanyDetail}
              options={{
                headerShown: false,
                gestureEnabled: true,
                gestureDirection: 'vertical-inverted',
                transitionSpec: {
                  open: config,
                  close: closeConfig,
                },
              }}
            />
          </React.Fragment>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const mapStateToProps = (state) => {
  const {
    user: {isAuth},
  } = state;
  return {isAuth};
};

export default connect(mapStateToProps, null)(App);
