---
id: doc1
title: Explicando componentes
sidebar_label: Explicando componentes
---

## Buttons

Componente utilizado para ser um botão personalisável, podendo passar `props` assim sendo flexível para reutilização.

## Card

Componente utilizado para renderizar a lista de empresas retornada da API, mostrando principais informações sobre as empresas.

## Filter

Componente utilizado para filtrar uma lista de itens, podendo ser por categoria ou pesquisa por texto.

## Picker

Componente utilizado para selecionar em lista de itens uma opção.

## Picker

Componente utilizado para renderizar um `Spinner` enquanto requisições são realizadas.

## TextInput

Componente utilizado para renderizar um Input com o `label`.
