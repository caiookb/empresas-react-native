---
id: doc2
title: Explicando as Views
---

## Company

Tela utilizada para exibir a lista de empresas retornadas da API

## CompanyDetail

Tela utilizada para exibir os detalhes de cada Company selecionado.

## Login

Tela utilizada para renderizar um formulário de acesso para o usuário conseguir entrar na aplicação.
