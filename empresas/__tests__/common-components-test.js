/**
 * @format
 */

import React from 'react';
import {Card, Filter, Spinner, TextInput} from '../src/commom-components';
import {shallow} from 'enzyme';

describe('Examining if the components is rendered', () => {
  it('check if card is rendered', () => {
    const wrapper = shallow(<Card />);
    expect(wrapper.find({testId: 'card'}).exists());
  });

  it('check if search input rendered', () => {
    const wrapper = shallow(<Filter />);
    expect(wrapper.find({testId: 'filter-test'}).exists());
  });

  it('check if picker is rendered', () => {
    const wrapper = shallow(<Filter />);
    expect(wrapper.find({testId: 'filter-picker'}).exists());
  });

  it('check if spinner is rendered', () => {
    const wrapper = shallow(<Spinner />);
    expect(wrapper.find('ActivityIndicator').exists());
  });

  it('check if spinner is rendered', () => {
    const wrapper = shallow(<TextInput />);
    expect(wrapper.find({testId: 'input-test'}).exists());
  });
});
