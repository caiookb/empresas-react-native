/**
 * @format
 */

import React from 'react';
import Login from '../src/views/login/Login';
import {shallow} from 'enzyme';

it('renders correctly', () => {
  const wrapper = shallow(<Login />);
  expect(wrapper.find('View'));
});
