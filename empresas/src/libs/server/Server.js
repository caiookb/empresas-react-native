import AsyncStorage from '@react-native-community/async-storage';

const domain = {
  PRODUCTION: 'https://empresas.ioasys.com.br/api/v1/',
};

const urlPrefix = domain.PRODUCTION;

const url = (path) => {
  return urlPrefix.concat(path.join('/'));
};

const checkResponse = (save, response) =>
  save ? {headers: response.headers, user: response.json()} : response.json();

export default (config) => {
  const {method, path, body, auth, saveHeaders} = config;

  const opt = {
    headers: {
      'Content-Type': 'application/json',
      uid: `${auth && auth.uid}`,
      client: `${auth && auth.client}`,
      'access-token': auth && auth.accessToken,
    },
    method,
    body: body && JSON.stringify(body),
  };

  return fetch(url(path), opt)
    .then((res) => (res.ok ? checkResponse(saveHeaders, res) : res.json()))
    .catch((err) => Promise.reject(err));
};
