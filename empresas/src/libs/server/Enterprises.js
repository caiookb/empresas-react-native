import fetchServer from './Server';

export const getCompanies = (auth) => {
  return fetchServer({
    method: 'GET',
    path: ['enterprises'],
    auth,
  });
};

export const getCompanyDetail = (id, auth) => {
  return fetchServer({
    method: 'GET',
    path: ['enterprises', `${id}`],
    auth,
  });
};

export const getCompanyByType = (id, auth) => {
  return fetchServer({
    method: 'GET',
    path: ['enterprises', id === 0 ? '' : `?enterprise_types=${id}`],
    auth,
  });
};

export const getCompanyByName = (name, auth) => {
  return fetchServer({
    method: 'GET',
    path: ['enterprises', `?name=${name}`],
    auth,
  });
};
