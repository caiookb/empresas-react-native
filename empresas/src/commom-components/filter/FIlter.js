import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import {CompanyController} from '../../controllers';
import {TextInput} from '../index';
import CustomPicker from '../picker/Picker';

const Filter = (props) => {
  const {value, onChange, list} = props;
  const [types, setTypes] = useState([]);

  const filterByText = (text) => CompanyController.getCompaniesByName(text);
  const filterByType = (type) => CompanyController.getCompaniesByType(type);

  useEffect(() => {
    const typeArray = [];
    list?.map((res) =>
      typeArray.push({
        id: res.enterprise_type.id,
        type: res.enterprise_type.enterprise_type_name,
      }),
    );
    const uniqueTypes = [
      ...new Set(
        typeArray
          .sort((a, b) => a.id > b.id)
          .map((item) => JSON.stringify(item)),
      ),
    ].map((item) => JSON.parse(item));
    setTypes(uniqueTypes);
  }, [list]);

  return (
    <View>
      <TextInput
        value={value}
        onChange={onChange}
        filter={filterByText}
        placeholder={'Search by name'}
        testId={'filter-test'}
      />
      <CustomPicker
        testId={'filter-picker'}
        list={types}
        filter={filterByType}
      />
    </View>
  );
};

export default Filter;
