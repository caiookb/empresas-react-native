import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/FontAwesome';

const Card = (props) => {
  const {company, navigation, fetchDetail, key, testId} = props;
  return (
    <TouchableOpacity
      testID={testId}
      style={styles.card}
      onPress={() => {
        fetchDetail(company.id);
        navigation.navigate('company');
      }}
      key={key}>
      <View style={styles.imageView}>
        <Image
          style={styles.image}
          source={{
            uri: `https://empresas.ioasys.com.br${company?.photo}`,
          }}
        />
      </View>
      <View style={styles.data}>
        <Text style={styles.title}>{company?.enterprise_name}</Text>
        <Text style={styles.commomItem}>
          <Icon name="globe" size={16} color="black" /> {company?.city}/
          {company?.country}
        </Text>
        <Text style={styles.commomItem}>
          <Icon name="building-o" size={16} color="black" />{' '}
          {company?.enterprise_type.enterprise_type_name}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default Card;
