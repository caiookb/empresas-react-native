import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  card: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
    flexDirection: 'row',
    fontSize: 22,
    width: '90%',
    marginVertical: 5,
    borderColor: '#7070703a',
    borderWidth: 1,
    alignSelf: 'center',
    alignItems: 'center',
  },
  image: {
    width: 100,
    height: 100,
    borderWidth: 2,
    borderColor: '#7070703a',
    borderRadius: 5,
  },
  imageView: {
    fontWeight: '200',
    fontSize: 18,
    color: '#707070',
  },
  data: {
    flex: 1,
    height: '100%',
    marginLeft: 10,
    alignSelf: 'flex-start',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  commomItem: {
    marginTop: 5,
    fontWeight: '300',
  },
});
