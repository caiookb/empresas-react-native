import React from 'react';
import {View, Text, ActivityIndicator, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    marginTop: 200,
  },
});

const ActivitySpinenr = (props) => {
  const {text} = props;

  return (
    <View style={styles.container}>
      <ActivityIndicator size="large" color="#707070" testID={'spinner'} />
    </View>
  );
};

export default ActivitySpinenr;
