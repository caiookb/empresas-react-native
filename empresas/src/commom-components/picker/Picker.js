import React, {useState} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {Picker} from '@react-native-community/picker';

const CustomPicker = (props) => {
  const {list, filter, testId} = props;
  const [selectedValue, setSelectedValue] = useState('');
  return (
    <View style={styles.container}>
      <Text>Search by type:</Text>
      <Picker
        testID={testId}
        selectedValue={selectedValue}
        style={{height: 50, width: '70%'}}
        onValueChange={(itemValue) => {
          filter(itemValue);
          setSelectedValue(itemValue);
        }}>
        <Picker.Item label={'--'} value={0} />

        {list.map((item, key) => (
          <Picker.Item key={key} label={item.type} value={item.id} />
        ))}
        <Picker.Item label={"I don't have preference"} value={0} />
      </Picker>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default CustomPicker;
