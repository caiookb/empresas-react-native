import {StyleSheet} from 'react-native';
export default StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
  },

  buttonsText: {
    fontWeight: '200',
    textAlign: 'center',
    fontSize: 20,
    color: 'black',
  },
});
