import React, {Component, version} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import styles from './styles';

export const CustomButton = (props) => {
  const {color, onPress, title, type, isValid} = props;
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.button, {backgroundColor: color}]}
      disabled={isValid}>
      <Text style={styles.buttonsText}> {title} </Text>
    </TouchableOpacity>
  );
};

CustomButton.defaultProps = {
  height: 35,
  rounded: false,
  onPress: () => {},
  onLongPress: () => {},
  style: {},
  raised: true,
  disabled: false,
  children: null,
};
