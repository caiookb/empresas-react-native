import React from 'react';
import {View, Text, TextInput as Input} from 'react-native';
import styles from './styles';

const Label = ({label}) => (
  <Text style={styles.label}>
    <Text style={styles.labelText}>{label}</Text>
  </Text>
);

export const FormInput = (props) => {
  const {value, type, label, handleChange, name, error} = props;

  return (
    <View style={styles.textInput}>
      <Label label={label} />
      <Input
        style={styles.input}
        value={value}
        secureTextEntry={type === 'password' ? true : false}
        keyboardType={type === 'email' ? 'email-address' : 'default'}
        autoCapitalize={'none'}
        autoCompleteType={type}
        onChangeText={handleChange(`${name}`)}
      />
      <Text style={{fontSize: 14, color: 'red'}}>{error}</Text>
    </View>
  );
};

export const TextInput = (props) => {
  const {value, label, onChange, filter, placeholder, testId} = props;

  return (
    <View style={styles.textInput}>
      <Label label={label} />
      <Input
        style={styles.input}
        value={value}
        keyboardType={'default'}
        onChangeText={(text) => {
          onChange(text);
          filter(text);
        }}
        placeholder={placeholder}
        testID={testId}
      />
    </View>
  );
};

FormInput.defaultProps = {
  value: null,
  type: 'name',
  label: '',
};

TextInput.defaultProps = {
  filter: () => null,
};
