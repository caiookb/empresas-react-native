import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  textInput: {
    alignSelf: 'center',
    fontSize: 22,
    width: '90%',
    marginVertical: 20,
  },
  label: {
    marginHorizontal: 0,
  },
  labelText: {
    fontWeight: '200',
    fontSize: 18,
    color: '#707070',
  },
  input: {
    borderBottomColor: '#7070703a',
    borderBottomWidth: 1,
    paddingVertical: 5,
  },
});
