import React, {useEffect, useState} from 'react';
import {AuthController, CompanyController} from '../../controllers';
import {connect} from 'react-redux';
import Card from '../../commom-components/cards/Card';
import Filter from '../../commom-components/filter/FIlter';
import {Spinner} from '../../commom-components';
import {Text, TouchableOpacity, View, ScrollView} from 'react-native';

const Companies = (props) => {
  const {companies, user, navigation} = props;
  const [search, setSearch] = useState('');
  const [fetching, setFetching] = useState(false);

  const name = user?.investor?.investor_name;

  useEffect(() => {
    CompanyController.getCompanies();
  }, [user]);

  const getCompanyDetailById = (id) => CompanyController.getCompanyDetail(id);

  return (
    <ScrollView>
      <TouchableOpacity
        style={{padding: 20, alignSelf: 'flex-end'}}
        onPress={() => AuthController.logout(navigation)}>
        <Text>Logout</Text>
      </TouchableOpacity>
      <View style={{paddingHorizontal: 20}}>
        <Text style={{fontSize: 26}}>
          Welcome{' '}
          <Text testID={'username'} style={{fontWeight: 'bold'}}>
            {name}
          </Text>
        </Text>
      </View>
      <Filter value={search} onChange={setSearch} list={companies} />
      {fetching ? (
        <Spinner />
      ) : (
        companies?.map((company, key) => (
          <Card
            navigation={navigation}
            company={company}
            fetchDetail={getCompanyDetailById}
            key={key}
            testID={'card'}
          />
        ))
      )}
    </ScrollView>
  );
};

const mapDispatchToProps = (dispatch) => ({});

const mapStateToProps = (state) => {
  const {
    companies: {companies},
    user: {user},
  } = state;
  return {companies, user};
};

export default connect(mapStateToProps, mapDispatchToProps)(Companies);
