import React from 'react';
import {Text, View, ImageBackground, ScrollView} from 'react-native';
import {connect} from 'react-redux';
import styles from './styles';
import Icon from 'react-native-vector-icons/FontAwesome';

const CompanyDetail = (props) => {
  const {company, navigation} = props;
  return (
    <View style={styles.container}>
      <ImageBackground
        style={[styles.background]}
        source={{
          uri: `https://empresas.ioasys.com.br${company.photo}`,
        }}
      />
      <ScrollView contentContainerStyle={styles.data}>
        <Text style={styles.title}>{company?.enterprise_name}</Text>
        <Text style={styles.commomItem}>
          <Icon name="globe" size={22} color="black" /> {company.city}/
          {company.country}
        </Text>
        <Text style={styles.commomItem}>
          <Icon name="building-o" size={22} color="black" />
          {'  '}
          {company?.enterprise_type?.enterprise_type_name}
        </Text>

        <Text style={styles.commomItem}>
          <Icon name="money" size={22} color="green" /> Share price: $
          {company?.share_price}
        </Text>
        <Text style={styles.commomItem}>
          Description:
          {'\n\n'}
          {company?.description}
        </Text>
      </ScrollView>
      <Icon
        name="long-arrow-left"
        size={50}
        color="black"
        onPress={() => navigation.goBack()}
      />
    </View>
  );
};

const mapStateToProps = (state) => {
  const {
    companies: {selected_company},
  } = state;
  return {company: selected_company};
};

export default connect(mapStateToProps, null)(CompanyDetail);
