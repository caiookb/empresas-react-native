import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
    flexDirection: 'column',
    fontSize: 22,
    width: '90%',
    marginVertical: 5,
    alignSelf: 'center',
    alignItems: 'center',
  },
  background: {
    width: '100%',
    height: 300,
    borderWidth: 2,
    borderColor: '#7070703a',
    borderRadius: 5,
  },
  imageView: {
    fontWeight: '200',
    fontSize: 18,
    color: '#707070',
  },
  data: {
    flex: 1,
    height: '100%',
    alignSelf: 'flex-start',
    marginTop: 20,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  commomItem: {
    marginTop: 15,
    fontWeight: '300',
  },
});
