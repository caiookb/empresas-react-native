import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  logo: {
    alignSelf: 'center',
    marginVertical: 40,
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
});
