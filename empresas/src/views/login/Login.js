import React from 'react';
import {View, Image} from 'react-native';
import {Formik} from 'formik';
import {CustomButton, FormInput, TextInput} from '../../commom-components';
import {logo} from '../../assets/images';
import {AuthController} from '../../controllers';
import * as Yup from 'yup';
import styles from './styles';

const Login = (props) => {
  const {navigation} = props;
  return (
    <View style={styles.container}>
      <Image style={styles.logo} source={logo} />
      <Formik
        initialValues={{email: '', password: ''}}
        validationSchema={() =>
          Yup.object().shape({
            email: Yup.string()
              .email('Invalid email')
              .required('Required field'),
            password: Yup.string().required('Required field'),
          })
        }
        onSubmit={(values) => {
          AuthController.login(values);
          navigation.navigate('companies');
        }}>
        {({values, handleChange, errors, handleSubmit, isValid}) => (
          <View>
            <FormInput
              value={values.email}
              label={'Email'}
              type={'email'}
              name={'email'}
              handleChange={() => handleChange('email')}
              error={errors.email}
            />
            <FormInput
              value={values.password}
              label={'Password'}
              type={'password'}
              name={'password'}
              handleChange={() => handleChange('password')}
              error={errors.password}
            />
            <CustomButton
              onPress={isValid ? handleSubmit : null}
              title="Login"
              color={'#7070703a'}
            />
          </View>
        )}
      </Formik>
    </View>
  );
};

export default Login;
