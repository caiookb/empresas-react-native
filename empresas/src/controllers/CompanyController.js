import AsyncStorage from '@react-native-community/async-storage';
import {AuthController, StoreController} from '.';
import {CompanyActions} from '../libs/redux/actions';
import {CompaniesServer} from '../libs/server';

export const getCompanies = async () => {
  const headers = await AuthController.getToken();
  const dispatch = StoreController.dispatch();
  CompaniesServer.getCompanies(headers)
    .then((res) => {
      dispatch(CompanyActions.saveCompanies(res.enterprises));
    })
    .catch((error) => {
      throw error;
    });
};

export const getCompaniesByType = async (id) => {
  const headers = await AuthController.getToken();
  const dispatch = StoreController.dispatch();

  CompaniesServer.getCompanyByType(id, headers)
    .then((res) => {
      dispatch(CompanyActions.saveCompanies(res.enterprises));
    })
    .catch((error) => {
      throw error;
    });
};

export const getCompaniesByName = async (name) => {
  try {
    const headers = await AuthController.getToken();
    const dispatch = StoreController.dispatch();
    const res = await CompaniesServer.getCompanyByName(name, headers);
    dispatch(CompanyActions.saveCompanies(res.enterprises));
  } catch (error) {
    throw error;
  }
};

export const getCompanyDetail = async (companyId) => {
  const headers = await AuthController.getToken();
  const dispatch = StoreController.dispatch();
  CompaniesServer.getCompanyDetail(companyId, headers)
    .then((res) => {
      dispatch(CompanyActions.saveCompanyDetail(res.enterprise));
    })
    .catch((error) => {
      throw error;
    });
};

export const updateCompaniesFiltered = (companies) => {
  const dispatch = StoreController.dispatch();
  dispatch(CompanyActions.saveCompanies(companies));
};
