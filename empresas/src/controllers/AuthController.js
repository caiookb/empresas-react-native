import AsyncStorage from '@react-native-community/async-storage';
import {StoreController} from '.';
import {CompanyActions, UserActions} from '../libs/redux/actions';
import {AuthServer, CompaniesServer} from '../libs/server';

export const getToken = async () =>
  JSON.parse(await AsyncStorage.getItem('auth'));

export const saveHeadersOnDevice = async (headers) => {
  await AsyncStorage.setItem('auth', headers);
};

export const login = (data) => {
  const dispatch = StoreController.dispatch();

  return AuthServer.login(data)
    .then(async (response) => {
      const {client, uid} = response.headers.map;
      const user = await response.user;

      dispatch(UserActions.isAuth(true));
      dispatch(UserActions.saveUser(user));

      const headers = {
        client,
        uid,
        accessToken: response.headers.map['access-token'],
      };

      await saveHeadersOnDevice(JSON.stringify(headers));
    })
    .catch((error) => {
      throw error;
    });
};

export const logout = async (navigation) => {
  const dispatch = StoreController.dispatch();
  await AsyncStorage.removeItem('auth');
  dispatch(UserActions.clearUser());
  dispatch(UserActions.isAuth(false));
  dispatch(CompanyActions.clearCompanies());
  navigation.navigate('login');
};

export const checkAuth = async () => {
  const headers = await AsyncStorage.getItem('auth');
  const auth = JSON.parse(headers);
  try {
    const checkToken = await CompaniesServer.getCompanies(auth);
    return checkToken.enterprises ? true : false;
  } catch (error) {
    throw error;
  }
};
