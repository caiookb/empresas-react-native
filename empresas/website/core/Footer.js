/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

class Footer extends React.Component {
  render() {
    return (
      <footer
        className="nav-footer"
        style={{color: 'white', padding: 20}}
        id="footer">
        <h4>Documentação feita com Docusaurus e editada por Caio Henrique</h4>
        <h4>
          <a style={{color: 'yellow'}} href={'https://github.com/caiookb'}>
            Github
          </a>
          <br></br> <br></br>
          <a style={{color: 'Yellow'}} href={'https://github.com/caiookb'}>
            Linkedin
          </a>
        </h4>
      </footer>
    );
  }
}

module.exports = Footer;
