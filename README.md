# Desafio - iosays

Projeto criado em React Native para realizar um desafio proposto pela _ioasys_ para uma vaga.

### Documentação explicando Views e Components 
- https://caiookb.github.io/ioasys/

### Principais bibliotecas utilizadas

- "**@react-native-community/async-storage"**: Utilizando lib da comunidade porque o AsyncStorage do Native vai ser depreciado;
- **"@react-native-community/picker"**: Utilizando lib da comunidade porque o Picker do Native vai ser depreciado;
- **@react-navigation/native":** Utilizando para pode integrar o React Native no React Navigation;

- **"@react-navigation/stack"** Utilizando para poder der uma Stack Navigator pro React Navigation;
  - Essas outras libs são obrigatorias para poder usar a React Navigation sem problemas
    - "react-native-gesture-handler"
      "react-native-masked-text":
      "react-native-safe-area-context":
      "react-native-screens":
- **"react-native-vector-icons"**: Biblioteca com presets de Icons;
- **"react-redux"/"redux"/"redux-thunk"** : Bibliotecas para o funcionamento do Redux na aplicação;
- **"formik"**: Biblioteca para composição de formulários de forma mais organizada;
- **"yup"**: Bilbioteca utilizada para fazer a validação de uma forma prática de formulários
- **"enzime"**: Biblioteca utilizada para realizar testes com jest
  - Essas outras libs são obrigatorias para poder usar o Enzime sem problemas:
    - **"enzyme-adapter-react-16"**;
    - **"jest-environment-enzyme"**;
    - **"jest-enzyme"**

### Como executar a aplicação

- Pré-requisitos - NodeJs; - Android Studio (caso for rodar no emulador)

1 - Se você deseja rodar a aplicação em um Emulador de Android, primeiro será necessário preparar o ambiente para execução com o link a seguir: https://reactnative.dev/docs/environment-setup

2 - Depois que preapar todo o ambiente, clone o repositório e execute o comando `npm install` para instalar todas as bibliotecas necessárias para funcionamento do projeto.

3 - Com o celular conectado via usb **ou** o Emulador rodando no seu computador, execute o comando `react-native run-android`, assim instalando e executando a aplicação em algum desses dispositivo.

4 - Caso a aplicação não execute após o `react-native run-android`, execute o comando `npm start` e abra a aplicação logo após a execução da commandline.
Muito obrigado pela oportunidade de realizar o desafio :)
Qualquer dúvida pode entrar em contato no caiookb@gmail.com
